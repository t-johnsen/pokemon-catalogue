# Pokémon Catalogue

This is a pokémon catalogue where as a user you can enter your trainer name and find and collect your pokémons.
##### [Try it out here!]
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.2.

## Setup
Clone reposityory to your local machine. From the root folder run `npm install`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` (or `ng g c component-name`) to generate a new component. 
You can also use:
```
ng generate directive|pipe|service|class|guard|interface|enum|module
```

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

[//]: #
[Try it out here!]: <https://your-pokemon-app.netlify.app/>