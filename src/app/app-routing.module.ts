import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth/auth.guard';
import { ProviderGuard } from './guards/auth/provider.guard';
import { DetailedPokemonComponent } from './pages/detailed-pokemon/detailed-pokemon.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { PokemonsComponent } from './pages/pokemons/pokemons.component';
import { TrainerComponent } from './pages/trainer/trainer.component';

const routes: Routes = [
  {
    path: 'landing',
    component: LandingPageComponent,
    canActivate: [ ProviderGuard ]
  },
  {
    path: 'pokemon',
    component: PokemonsComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'trainer',
    component: TrainerComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'pokemon/:id',
    component: DetailedPokemonComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/landing'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
