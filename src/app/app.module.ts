import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { DetailedPokemonComponent } from './pages/detailed-pokemon/detailed-pokemon.component';
import { PokemonsComponent } from './pages/pokemons/pokemons.component';
import { ProfileCardComponent } from './components/profile-card/profile-card.component';
import { BaseStatsComponent } from './components/base-stats/base-stats.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { MovesComponent } from './components/moves/moves.component';
import { CatalogueCardComponent } from './components/catalogue-card/catalogue-card.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { TrainerComponent } from './pages/trainer/trainer.component';

@NgModule({
  declarations: [
    AppComponent,
    DetailedPokemonComponent,
    PokemonsComponent,
    ProfileCardComponent,
    BaseStatsComponent,
    NavbarComponent,
    MovesComponent,
    CatalogueCardComponent,
    FooterComponent,
    LandingPageComponent,
    TrainerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
