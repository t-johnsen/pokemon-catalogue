import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-stats',
  templateUrl: './base-stats.component.html',
  styleUrls: ['./base-stats.component.css']
})
export class BaseStatsComponent implements OnInit {
  @Input() name: string;
  @Input() sprite: string;
  @Input() stats: [];
  @Input() types: [];
  constructor() { }

  ngOnInit(): void {

  }

}
