import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-catalogue-card',
  templateUrl: './catalogue-card.component.html',
  styleUrls: ['./catalogue-card.component.css']
})
export class CatalogueCardComponent implements OnInit {
  @Input() name: string;
  @Input() sprite: string;
  @Output() clicked: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onPokemonClicked(): void {
    this.clicked.emit(this.name);
  }

}
