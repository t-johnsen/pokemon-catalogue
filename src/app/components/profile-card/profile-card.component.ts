import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.component.html',
  styleUrls: ['./profile-card.component.css']
})
export class ProfileCardComponent implements OnInit {

  @Input() height: number;
  @Input() weight: number;
  @Input() abilities: [];
  @Input() baseExperience: number;
  constructor() { }

  ngOnInit(): void {
  }

}
