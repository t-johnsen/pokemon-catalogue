import { Component, OnInit } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon/pokemon.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(
    private pokemonService: PokemonService
  ) { }

  ngOnInit(): void {
  }

  onPreviosClicked(){
    this.pokemonService.previousPage();
  }

  onNextClicked(){
    this.pokemonService.nextPage();
  }

}
