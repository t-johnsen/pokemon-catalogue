export interface PokemonSimplified {
    name: string,
    url: string,
    sprite?: string
}