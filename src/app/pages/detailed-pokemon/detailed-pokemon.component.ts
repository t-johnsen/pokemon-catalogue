import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/services/pokemon/pokemon.service';

@Component({
  selector: 'app-detailed-pokemon',
  templateUrl: './detailed-pokemon.component.html',
  styleUrls: ['./detailed-pokemon.component.css']
})
export class DetailedPokemonComponent implements OnInit {
  pokemon: Pokemon;
  private id: string;
  isAddedToCollection: boolean = false;
  isAlreadyAddedToCollection: boolean = false;
  disableCollectButton: boolean = true;

  constructor(
    private router: ActivatedRoute,
    private pokemonService: PokemonService
  ) {
    this.id = this.router.snapshot.params.id;
  }

  async ngOnInit(): Promise<void> {
    try {
      this.pokemon = await this.pokemonService.getOnePokemon(this.id);
    } catch (error) {
      console.log(error);
    }
    this.disableCollectButton = false;
  }

  collectPokemonClicked() {
    const sprite = this.pokemon?.sprites.other['official-artwork'].front_default;
    const name = this.pokemon?.name;
    //if(name == null || name == undefined || sprite == undefined || sprite == undefined) this.disableCollectButton = true;
    if (this.pokemonService.addPokemonToCollection(name, sprite)) {
      this.isAddedToCollection = true;
      this.isAlreadyAddedToCollection = false;
    }else{
      this.isAddedToCollection = false;
      this.isAlreadyAddedToCollection = true;
    }
  }

}
