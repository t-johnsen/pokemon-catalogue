import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {
  trainerName: string;

  constructor(
    private router: Router 
  ) { }

  ngOnInit(): void {

  }

  goClicked(){
    if(this.trainerName == undefined || this.trainerName == '' || this.trainerName.trim() == '' || this.trainerName.length < 2) return;
    this.router.navigate(["pokemon"]);
    localStorage.setItem('Trainer', this.trainerName);
  }

}
