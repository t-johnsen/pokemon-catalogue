import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PokemonSimplified } from 'src/app/models/pokemon-simplified.model';
import { PokemonService } from 'src/app/services/pokemon/pokemon.service';

@Component({
  selector: 'app-pokemons',
  templateUrl: './pokemons.component.html',
  styleUrls: ['./pokemons.component.css']
})
export class PokemonsComponent implements OnInit, OnDestroy {
  
  public pokemons: PokemonSimplified[];
  private firstURL: string = 'https://pokeapi.co/api/v2/pokemon?limit=20&offset=0';
  pokemonUrl: string;
  private pokemons$: Subscription;

  constructor( 
    private pokemonService: PokemonService,
    private router: Router 
   ) {
     this.pokemons$ = this.pokemonService.getPokemonsPagination$().subscribe( val =>{
       this.pokemons = val;
     });
    }

  async ngOnInit(): Promise<void> {
    try {
      this.pokemons = await this.pokemonService.getPokemons(this.firstURL);
    } catch (error) {
      console.log(error);
    }
  }

  handlePokemonClicked(name: string): void{
    console.log(name);
    this.router.navigate(["/pokemon", name]);
  }

  ngOnDestroy() {
    this.pokemons$.unsubscribe();
  }
/*   private helpGetId: void(){

  } */
  //Onclick card --> get url from selected pokemon and store in var. substring and slice it
  // to get the number and use link 

}
