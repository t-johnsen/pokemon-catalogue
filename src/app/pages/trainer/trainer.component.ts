import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonService } from 'src/app/services/pokemon/pokemon.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {

  pokemonCollection: any;
  searchCollection: any;
  searchString: string = '';
  currentSprite: string = '../../../assets/pokemon.png';
  currentName: string;

  constructor(
    private pokemonService: PokemonService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.pokemonCollection = this.pokemonService.getPokemonsFromCollection();
    this.searchCollection = this.pokemonService.getPokemonsFromCollection();
  }

  onPokemonClicked(event: any): void {
    console.log(event.target.value);
    let currPokemon = this.pokemonCollection.find(pokemon => pokemon.name == event.target.value);
    this.currentSprite = currPokemon.sprite;
    this.currentName = currPokemon.name;

  }

  goDetailsClicked(): void {
    this.router.navigate(["/pokemon", this.currentName]);
  }

  onKey(value: string) {
    this.searchString = value;
    this.searchCollection = this.pokemonCollection.filter(pokemon => pokemon.name.slice(0, this.searchString.length) == this.searchString);
  }
}
