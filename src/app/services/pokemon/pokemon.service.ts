import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonSimplified } from 'src/app/models/pokemon-simplified.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private pokemons$: BehaviorSubject<PokemonSimplified[]> = new BehaviorSubject([]);

  private baseURL: string = 'https://pokeapi.co/api/v2/pokemon';
  nextPaginationUrl: string;
  prevPaginationUrl: string;
  private pokemons: PokemonSimplified[];

  constructor(
    private http: HttpClient
  ) { }

  getPokemonsPagination$(): Observable<PokemonSimplified[]> {
    return this.pokemons$.asObservable();
  }

  async previousPage() {
    try {
      this.pokemons = await this.getPokemons(this.prevPaginationUrl);
    } catch (error) {
      console.log(error);
    }
    this.pokemons$.next(
      this.pokemons
    );
  }

  async nextPage() {
    try {
      this.pokemons = await this.getPokemons(this.nextPaginationUrl);
    } catch (error) {
      console.log(error);
    }
    this.pokemons$.next(
      this.pokemons
    );
  }

  getOnePokemon(value: any): Promise<Pokemon> {
    return this.http.get<Pokemon>(`${this.baseURL}/${value}`).toPromise();
  }

  getPokemons(paginationUrl: string): Promise<PokemonSimplified[]> {
    return this.http.get<any>(paginationUrl).pipe(

      tap((res: any) => {
        this.nextPaginationUrl = res.next;
        this.prevPaginationUrl = res.previous;
      }),

      map((res: any) => {
        return res.results.map((pokemon: PokemonSimplified) => {
          let part = pokemon.url.slice(34);
          let id = part.slice(0, part.length - 1);
          return {
            ...pokemon,
            sprite: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`
          }
        })
      })
    ).toPromise();
  }

  addPokemonToCollection(name: string, sprite: string): boolean {
    let pokemonCollection = [];
    if (localStorage.getItem('PokemonCollection')) {
      pokemonCollection = JSON.parse(localStorage.getItem('PokemonCollection'));
      if(!(pokemonCollection.find(pokemon => pokemon.name == name) == undefined)) return false;

      pokemonCollection = [{ name, sprite }, ...pokemonCollection];

    } else {
      pokemonCollection = [{ name, sprite }];
    }
    localStorage.setItem('PokemonCollection', JSON.stringify(pokemonCollection));
    return true;
  }

  getPokemonsFromCollection(): [] {
    if(!localStorage.getItem('PokemonCollection')) return;
    return JSON.parse(localStorage.getItem('PokemonCollection'));
  }

}
